import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

const state = {
    users: [],
    pagination: {
        total: 0,
        perPage: 5,
        currentPage: 0,
        lastPage: 0,
    },
    checkedIds: [],
};

const mutations = {
    setUsersData (state, data) {
        state.users = data.data;
        state.pagination.total = data.total;
        state.pagination.perPage = data.per_page;
        state.pagination.currentPage = data.current_page;
        state.pagination.lastPage = data.last_page;
    },
    clearCheckedIds (state) {
        state.checkedIds = [];
    },
    updateCheckedIds (state, checkedIds) {
        state.checkedIds = checkedIds;
    },
};

const actions = {
    getUsers: ({ commit }, page) => {
        page = page !== 0 ? page : 1;
        axios.get('/api/users', {
            params: {page}
        })
            .then((response) => commit('setUsersData', response.data))
    },

};

const getters = {
    checkedIdsCount: state => {
        return state.checkedIds.length;
    }
};

export default new Vuex.Store({
    state,
    getters,
    actions,
    mutations
})
