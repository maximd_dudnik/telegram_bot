import Vue from 'vue';
import axios from 'axios';
import router from './router/';
import App from './components/App.vue';
import Paginate from 'vuejs-paginate';
import store from './store';

Vue.component('paginate', Paginate);

Vue.prototype.$http = axios.create();

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
});
