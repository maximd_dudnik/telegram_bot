<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function () {
    // Users get
    Route::get('/users', 'UserController@get');

    // Users delete
    Route::delete('/users', 'UserController@deleteMany');
});

Route::post('/bot', 'BotController@processCommand');
Route::post('/send-message', 'BotController@sendMessage');

Route::get('/{view?}', 'HomeController@index')->where('view', '(.*)');
