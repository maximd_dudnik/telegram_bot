<?php

namespace App\Services\Bot;

use App\User;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use Spatie\SslCertificate\SslCertificate;
use BotMan\Drivers\Telegram\TelegramDriver;

/**
 * Class BotService.
 * @package App\Services
 */
class BotService
{
    /**
     * @var BotManFactory
     */
    private $botman;

    /**
     * BotService constructor.
     */
    public function __construct()
    {
        DriverManager::loadDriver(TelegramDriver::class);
        $config = [
            'telegram' => [
                'token' => env('TELEGRAM_TOKEN'),
            ]
        ];

        // create an instance
        $this->botman = BotManFactory::create($config);
    }

    /**
     * Process telegram command
     */
    public function processCommand()
    {
        $this->botman->hears('ssl-info {domain}', function (BotMan $bot, $domain) {
            $this->proccessSSLCheck($bot, $domain);
        });

        $this->botman->hears('subscribe', function (BotMan $bot) {
            $this->processSubscribe($bot);
        });

        $this->botman->hears('unsubscribe', function (BotMan $bot) {
            $this->processUnsubscribe($bot);
        });

        $this->botman->fallback(function(BotMan $bot) {
            $bot->types();
            $bot->reply('Sorry, I did not understand these commands. Please retype again...');
        });

        // start listening
        $this->botman->listen();
    }

    /** Send message to subscribers
     *
     * @param string|null $message - Message text
     * @param array $selectedUsers - Selected users
     *
     * @return void
     */
    public function sendMessage(string $message = null, array $selectedUsers)
    {
        $users = User::whereIn('telegram_id', $selectedUsers)->get();

        foreach ($users as $user) {
            $this->botman->say($message, $user->telegram_id, TelegramDriver::class);
        }

        info('Sent message to subscribers');
    }

    /**
     * @param BotMan $bot
     * @param string $domain
     */
    private function proccessSSLCheck(BotMan $bot, $domain)
    {
        try {
            $certificate = SslCertificate::createForHostName($domain);
            $isValid = $certificate->isValid() ? 'true' : 'false';

            $message = 'Issuer: ' . $certificate->getIssuer() . PHP_EOL;
            $message .= 'Is valid: ' . $isValid . PHP_EOL;
            $message .= 'Expires in: ' . $certificate->expirationDate()->diffInDays() . ' days';
            $bot->reply($message);
        } catch (\Exception $exception) {
            $bot->reply('Error! Check domain again.');
        }
    }

    /**
     * @param BotMan $bot
     */
    private function processSubscribe(BotMan $bot)
    {
        $telegramUserId = $bot->getUser()->getId();
        $telegramUserName = $bot->getUser()->getFirstName();

        $user = User::where('telegram_id', $telegramUserId)->first();
        if ($user) {
            $bot->reply('You are already subscribed');

            return;
        }

        try {
            User::create([
                'telegram_id' => $telegramUserId,
                'name' => $telegramUserName,
            ]);

            info('Successfully subscribed user with telegram_user_id', [$telegramUserId]);
            $bot->reply('Welcome ' . $telegramUserName . '.You have been subscribed');
        } catch (\Exception $exception) {
            $bot->reply('Error! Try again.');
        }
    }

    /**
     * @param BotMan $bot
     */
    private function processUnsubscribe(BotMan $bot)
    {
        $telegramUserId = $bot->getUser()->getId();
        $user = User::where('telegram_id', $telegramUserId)->first();
        if (!$user) {
            $bot->reply('You are already unsubscribed');

            return;
        }

        User::where('telegram_id', $telegramUserId)->delete();

        info('Successfully unsubscribed user with telegram_user_id', [$telegramUserId]);
        $bot->reply('You have been unsubscribed');
    }
}
