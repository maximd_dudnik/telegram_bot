<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\Bot\BotService;

/**
 * Class Bot
 *
 * @method static void processCommand(string $command = null)
 * @method static void sendMessage(string $message = null, array $selectedUsers)
 * @method static bool isValidCommand(string $command = null)
 *
 * @see BotService
 * @package App\Facades
 */
class Bot extends Facade
{
    /**
     * @inheritDoc
     */
    protected static function getFacadeAccessor()
    {
        return BotService::class;
    }
}
