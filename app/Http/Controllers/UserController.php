<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Get users
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function get()
    {
        return User::paginate(5);
    }

    /**
     * Delete many users
     *
     * @param Request $request
     *
     * @return bool
     */
    public function deleteMany(Request $request)
    {
        return User::whereIn('telegram_id', $request->id)->delete();
    }
}
