<?php

namespace App\Http\Controllers;

use App\Facades\Bot;
use Illuminate\Http\Request;

class BotController extends Controller
{

    /**
     * Process command
     */
    public function processCommand()
    {
        Bot::processCommand();
    }

    /**
     * Send message to subscribers
     *
     * @param Request $request
     */
    public function sendMessage(Request $request)
    {
        Bot::sendMessage($request->message, $request->id);
    }
}
